import Vue from 'vue'
import VueRouter from 'vue-router'
import Builder from "@/views/Builder";
import Home from "@/views/Home";
import Login from "@/views/Login";
import Registation from "@/views/Registation";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {auth: true},
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    meta: {auth: false},
    component: Login
  },
  {
    path: '/registration',
    name: 'Registration',
    meta: {auth: false},
    component: Registation
  },
  {
    path: '/constructor',
    name: 'Builder',
    meta: {auth: true},
    component: Builder
  },
  {
    path: '/constructor/:id',
    name: 'Builder',
    meta: {auth: true},
    component: Builder
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const data = localStorage.getItem('session')
  const session = JSON.parse(data)
  if (to.matched.some(record => record.meta.auth) && session == null){
    next({
      name: "Login",
      path: "/login"
    });
  } else {
    next()
  }
})

export default router
