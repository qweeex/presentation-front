import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueApexCharts from 'vue-apexcharts'
Vue.use(VueApexCharts)


// Import boostrap
import "bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"

import "./assets/style/mainCss.css";


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
